# Gemnasium DB

This repository contains the security advisories used by the 
[GitLab dependency scanners](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html).
It can be used for both searching advisories and submitting new ones.
The GitLab team constantly improves this vulnerability database
by checking external sources on a regular basis, and contributing their findings to this repo.
Learn more on the [external sources](SOURCES.md) and how the GitLab team tracks them.

Detailed statistics about the security advisories that are contained in this repository 
are available at [the project homepage](https://gitlab-org.gitlab.io/security-products/gemnasium-db/). 

## Licensing

This vulnerability database is available freely under the 
[GitLab Security Alert Database Terms](./LICENSE.md), 
please review them before any usage.

## Contributing

If you know about a vulnerability that isn't listed in this repo,
you can contribute to the Gemnasium database by opening an issue,
or even submit the vulnerability as a YAML file in a merge request.
Please review the [contribution guidelines](CONTRIBUTING.md).

## Publishing

The maintainers of this project use the
[gemnasium-db-toolbox](https://gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox)
command line tool to maintain this repository.

## Directory structure

Each YAML file of the repo corresponds to a single security advisory.
The file path is made of the slug of the affected package (type and name),
and the identifier of the advisory, following this pattern:

```
package_slug/advisory_identifier.yml
```

For instance, security advisory [CVE-2019-11324](https://nvd.nist.gov/vuln/detail/CVE-2019-11324)
related to Python package [urllib3](https://pypi.org/project/urllib3/) is:

```
pypi/urllib3/CVE-2019-11324.yaml
```

## Package slug

The package [slug](https://en.wikipedia.org/wiki/Clean_URL#Slug) is made of
the package type and the fully qualified package name separated by a slash `/`.

The supported package types are:

```
gem
go
maven
npm
packagist
pypi
```

These correspond to:

- gems from [rubygems.org](http://rubygems.org)
- go from source code hosting services such as `gitlab.com` or `github.com`
- Java Maven packages from [Maven Central](https://repo1.maven.org/maven2/)
- npm packages from [npmjs.com](https://www.npmjs.com/)
- PHP Composer packages from [packagist.org](https://packagist.org/)
- Python packages from [pypi.org](https://pypi.org/)

For go, the package name is the same as used by `go get` command for
downloading packages, e.g., `go get github.com/mholt/archiver/v3`.

For npm packages, the package name may include a [npm scope](https://docs.npmjs.com/misc/scope).
For instance, the package slug of [@babel/cli](https://www.npmjs.com/package/@babel/cli) is:

```
npm/@babel/cli
```

For Maven packages, the package name is made of the `groupId` and the `artifactId` separted by a slash `/`.
For instance, the package slug of [jackson-databind](https://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-databind/) is:

```
maven/com.fasterxml.jackson.core/jackson-databind
```

## YAML schema

* `identifier` should be the CVE id when it exists. If a vulnerability (and the
corresponding advisory) cannot be linked to a CVE, you can use our internal id
`GMS-<year>-<nr>` where `<year>` is the year in which the vulnerability was
disclosed and `<nr>` is a sequence number. In order to generate a new, unique
valid id, you can use our helper script `identifier/identifer.rb` which can be
invoked with `identifier/identifer.rb -n . <year>` where `<year>` is the year
in which the vulnerability has been disclosed, e.g. `2017`.
* `package_slug` (string): Package type and package name separated by a slash.
* `title` (string): A short description of the security flaw.
* `description` (string): A long description of the security flaw and the possible risks.
   The [CommonMark](https://spec.commonmark.org/0.28/) flavor of Markdown can be used.
* `pubdate` (string): The date on which the advisory was made public, in ISO-8601 format; `pubdate` refers to the publication date provided by the data-source from which the advisory originates (e.g., NVD).
* `date` (string): The last date on which the advisory was modified, in ISO-8601 format; `date` refers to the modification date provided by the data-source from which the advisory originates (e.g., NVD).
* `affected_range` (string): The range of affected versions. Machine-readable syntax used by the package manager.
* `affected_versions` (string): The range of affected versions. Human-readable version for display.
* `fixed_versions` (array of strings): The versions fixing the vulnerability. The order is not relevant.
* `not_impacted` (string, optional): Environments not affected by the vulnerability.
* `solution` (string, optional): How to remediate the vulnerability.
* `credit` (string, optional): The names of the people who reported the vulnerability or helped fixing it.
* `urls` (array of strings): URLs of: detailed advisory, documented exploit, vulnerable source code, etc.
   The order is not relevant.
* `cvss_v2` (string, optional): The CVSS attack vector (version 2.x) for a
given vulnerability (see https://www.first.org/cvss/v2/ for more details).
* `cvss_v3` (string, optional): The CVSS attack vector (version 3.x) for a
given vulnerability (see https://www.first.org/cvss/v3-1/ for more details).

`affected_range` is processed by Gemnasium when publishing the security advisory
whereas `affected_versions` is simply a string presented to users.

The syntax to be used in `affected_range` depends on the package type:
- `gem`: [gem requirement](https://guides.rubygems.org/specification-reference/#add_runtime_dependency)
- `maven`: [Maven Dependency Version Requirement Specification](https://maven.apache.org/pom.html#Dependency_Version_Requirement_Specification)
- `npm`: [node-semver](https://github.com/npm/node-semver#ranges)
- `php`: [PHP Composer version constraints](https://getcomposer.org/doc/articles/versions.md#writing-version-constraints)
- `pypi`: [PEP440](https://www.python.org/dev/peps/pep-0440/#version-specifiers)
- `go`: [go semver](https://godoc.org/golang.org/x/tools/internal/semver)

Our YAML file schema for advisories is specified in [schema/schema.json](schema/schema.json).

Here's a sample document:

```
---
identifier: CVE-2019-5477
package_slug: gem/nokogiri
title: Command Injection
description: "A command injection vulnerability in Nokogiri and earlier allows
  commands to be executed in a subprocess via Ruby's `Kernel.open` method. Processes
  are vulnerable only if the undocumented method `Nokogiri::CSS::Tokenizer#load_file`
  is being called with unsafe user input as the filename."
date: '2019-09-26'
affected_range: "<=1.10.3"
fixed_versions:
- 1.10.4
affected_versions: All versions up to 1.10.3
not_impacted: All versions after 1.10.3
solution: Upgrade to version 1.10.4 or above.
urls:
- https://nvd.nist.gov/vuln/detail/CVE-2019-5477
- https://github.com/sparklemotion/nokogiri/issues/1915
cvss_v2: AV:N/AC:L/Au:N/C:P/I:P/A:P
cvss_v3: CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
uuid: 7e9c16eb-d4ee-4c0a-a3b7-ed88f1ea7125
credit: Katsuhiko YOSHIDA
```
